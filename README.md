# First of all, thank you [Mr.Bau](https://gitlab.com/bautd.wiicamp)
# Demo UI for Intern Member

## I. Note:

### 1. About myself:

Blo, my name is Bau, a front-end developer at first role!

### 2. About this repository:

This file is for Wiicamp's new player, and you, if you want too. This guide will cover most of the issues, as well as the tasks that you need to follow while working in Wiicamp (may not be correct in some cases). Any contribution, help is welcome, just create a Request and I will check it. Thank you! Enjoy reading!

## II. Some rules that you have to remember when working at Wiicamp!

### 1. Task Manager:

At Wiicamp, most projects are managed using Trello. Here are some requirements for developers when you do a task on Trello:


### a. Before start coding:

1. Make sure you know all about your task.
2. Drag the task to the `Doing` column on Trello.
3. Comment with the following information for the task:
	```
	Dev: 
	Start Time:
	Estimate: 
	End time as expected:
	Real end time:
	Request:
	```
	For unclear information (`Request link`, `Real end time`), fill in `N/A`.
4. Start working now!

### b. After done coding:

1. If this is an interface task (FE), check the UI and see if it's responsive on all screens.
2. If this is a logical task, the function test has worked fine in all possible cases.
3. Create request and your comment in the task (`Request`).
4. Send request link to Maintainer/Leader, and ask them to take a look at the request.
5. Fix comment from Maintainer/Leader in the request. Back to step 5.
6. If the request is merged, update the comment in Trello (`Real end time`).
7. Take your next task and RE-DO!

### 2. Coding:
At Wiicamp, most projects are managed on Gitlab. Here are some requirements for developers when you do a task by coding:

### a. Before coding:
Every time you start working, always checkout from main/dev branch. Use `git pull origin main/dev` to get the latest lines of code.

### b. After done coding:

Check your code syntax is correct with the template rules.

1. HTML & CSS Rules: [Click Here](https://google.github.io/styleguide/htmlcssguide.html)
2. JS Rules: [Click Here](https://github.com/airbnb/javascript)

### 3. Git:
Reference flow: [A successful Git branching model](http://nvie.com/posts/a-successful-git-branching-model/)

We define here for a short guide.

### The main branches
The central repo holds two main branches with an infinite lifetime:
* `master`
* `develop`

Note:

1. We consider `origin/master` to be the main branch where the source code of HEAD always reflects a `production-ready` state.

2. We consider `origin/develop` to be the main branch where the source code of HEAD always reflects a state with the latest `delivered development changes for the next release`. This is also called `integration branch`.

### Supporting branches and branch naming rules
* Checkout from: `develop`
* Must merge back into: `develop`
* Branch naming convention:
  * Except `master`, `develop` and
  * Must be `tracker/trello_name_of_task`:
  * `Tracker`:
    * `task` for normal task, feature.
    * `bug` or `hotfix` for bug fixing.
    * `release` for release branch from `develop` to `master` for stable version.
  * `trello_name_of_task` for example: `FE_Create_XXX`...
  * Example: task/FE_Homepage_Create_UI_banner_welcome

### Commit message rules
* Format `refs *tracker* *Trello Name Of task*`
* Message: the short and meaningful purpose of the pull request

For Example:
* `refs task [FE] [Homepage] Create UI banner welcome`.
* `refs bug [BE] [API] [Notification] Fix bug when create new notification`.
* `refs hotfix [FE] [APP] Init config webpack and eslint`.

### Develop steps

From now, we will call central repository as `origin`.

1. Sync local's develop branch with origin's develop branch.
    ```sh
    $ git checkout develop
    $ git pull origin develop
    ```

2. Create new branch to do task from develop branch on local. Name of branch should be meaningful.(For example: `task/FE_Homepage_Create_UI_banner_welcome`)
    ```sh
    $ git checkout develop # <--- unnecessary in case already on develop branch
    $ git checkout -b task/FE_Homepage_Create_UI_banner_welcome
    ```

3. Coding and done your task with `ONLY 1 COMMIT`.

4. Move to local's develop branch and update to latest version.
    ```sh
    $ git checkout develop
    $ git pull origin develop
    ```

5. Move back to your task branch, rebase this branch with develop branch.
    ```sh
    $ git checkout task/FE_Homepage_Create_UI_banner_welcome
    $ git rebase develop
    ```
    **If conflict error occurs when rebasing, please refer to "fix conflict error when rebasing" procedure.**

6. Push to `origin`

    ```sh
    $ git push -u origin task/FE_Homepage_Create_UI_banner_welcome
    ```
   `-u` for the first time, after that no option is needed.

7. On Gitlab, create pull request from origin's  `task/FE_Homepage_Create_UI_banner_welcome`.

8. Paste pull request page's URL to chatwork group, ask reviewer to have code-review.

    8.1. If reviewer asks you to fix something, do 3. ~ 6. step.

    8.2. **Do not force push while reviewing. All commit messages (include the fix comment) must be meaningful.**
      
      * For example:
        * "fix comment" => Bad
        * "refs task [FE] [Homepage] Create UI banner welcome" => Good

    8.3. Re-paste pull request page's URL to chatwork group, ask reviewer to have code-review.
    
9. When more than 1 reviewer gave you OK comment, the pull request will be ready to merge.
10. Merge the accepted pull request.
11. Delete merged branch.
12. Back to step 1.

### Fix conflict error when rebasing

If conflict error occurs when rebasing, it will be displayed as below (at this moment, you will be moved to anonymous branch automatically).
```sh
$ git rebase develop
First, rewinding head to replay your work on top of it...
Applying: refs #1234 Can not remove cache
Using index info to reconstruct a base tree...
Falling back to patching base and 3-way merge...
Auto-merging path/to/conflicting/file
CONFLICT (add/add): Merge conflict in path/to/conflicting/file
Failed to merge in the changes.
Patch failed at 0001 refs #1234 Can not remove cache
The copy of the patch that failed is found in:
    /path/to/working/dir/.git/rebase-apply/patch

When you have resolved this problem, run "git rebase --continue".
If you prefer to skip this patch, run "git rebase --skip" instead.
To check out the original branch and stop rebasing, run "git rebase --abort".
```

1. Fix all the conflicts manually (code which is surrounded by <<< and >>>)
Use `git rebase --abort` if you want to abort rebase process.

2. After fixing all conflicts, continue your rebase process.

    ```sh
    $ git add .
    $ git rebase --continue
    ```

### 4. Create request:
All repositories have Request Template. Please fill in all the information in the description of the request.


